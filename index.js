//IMPORTS
const fs = require('fs')
const path = require('path')
const bodyParser = require('body-parser')
const csv = require('csv-parser')
const express = require('express')
const app = express()

//Renderer
const pug = require('pug')

//Express config
app.set('view engine', 'pug')
app.use(express.static('./views'));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(express.json())
app.use(express.urlencoded())

//Globals
const Armors = [];
var filteredArmors = [];

//Read Database CSV
fs.createReadStream('database/armordata.csv')
    .pipe(csv())
    .on('data', (data) => Armors.push(data))
    .on('end', () => {});

//ROUTES
app.post('/', (req,res) => {
    filteredArmors = [];
    let skills = []

    for (const f in req.body)
        if (f.includes("skill") && req.body[f] != "---Select Skill---")
            skills.push(req.body[f])
    
    Armors.forEach(armor => {
        let isCompliant = true
        skills.forEach(_skill => {
            if (!checkSkill(armor,_skill) ) isCompliant = false
        });
        
        if (req.body.huntertype != "Blade/Gunner")
            if(!armor["HunterType"].includes(req.body.huntertype))
                isCompliant = false

        if (armor["Rarity"] > parseInt(req.body.maxrarity) ) isCompliant = false

        if (isCompliant)
            filteredArmors.push(armor)
    });

    res.render('index', { filteredRarity: req.body.maxrarity, filteredSkills: skills, armors: filteredArmors, skillslist: ["---Select Skill---","Alchemy","All Resist","Anti-Theft","AntiChamel","AntiDaora","AntiFirDrg","Antiseptic","Artisan","Attack","Auto-Guard","AutoReload","Backpackng","BBQ","BombStrUp","Capacity","Carving","ClsRngCAdd","ClustS Add","Cold Res","ComradeAtk","ComradeDef","ComradeGuide","Constitution","Cooking","Crag S Add","Defence","Dragon Res","Edgemaster","ElementAtk","Evade","Evade Dist","Everlasting","Expert","Faint","Fate","Fatigue","Fencing","Fire Res","Fishing","Fury","Gathering","Gluttony","Guard","Guard Up","Gunnery","Guts","Health","HearProtection","Heat Res","HiSpdGathr","Horn","Hunger","Ice Res","Map","MixSucRate","NormalS Up","NormalSAdd","ParalyCAdd","Paralysis","PelletS Up","PelletSAdd","Perceive","PierceS Up","PierceSAdd","Poison","PoisonCAdd","Potential","PowerCAdd","Precision","Protection","PsychicVis","Quake Res","Rec Speed","Recoil","Recovery","Reload","ResistSts","Sharpness","ShortCharge","Shot Mix","Sleep","SleepCAdd","Sneak","Snow Res","Spc Attack","SpeedSetup","Stamina","SteadyHand","SwdShrpner","Sword Draw","Terrain","Throw","ThunderRes","Torso Inc","Tranquilzer","Water Res","Whim","Wide Area","WindPress"] })
    res.send()
})

app.get('/', (req,res) => {
    res.render('index', { armors: Armors, skillslist: ["---Select Skill---","Alchemy","All Resist","Anti-Theft","AntiChamel","AntiDaora","AntiFirDrg","Antiseptic","Artisan","Attack","Auto-Guard","AutoReload","Backpackng","BBQ","BombStrUp","Capacity","Carving","ClsRngCAdd","ClustS Add","Cold Res","ComradeAtk","ComradeDef","ComradeGuide","Constitution","Cooking","Crag S Add","Defence","Dragon Res","Edgemaster","ElementAtk","Evade","Evade Dist","Everlasting","Expert","Faint","Fate","Fatigue","Fencing","Fire Res","Fishing","Fury","Gathering","Gluttony","Guard","Guard Up","Gunnery","Guts","Health","HearProtection","Heat Res","HiSpdGathr","Horn","Hunger","Ice Res","Map","MixSucRate","NormalS Up","NormalSAdd","ParalyCAdd","Paralysis","PelletS Up","PelletSAdd","Perceive","PierceS Up","PierceSAdd","Poison","PoisonCAdd","Potential","PowerCAdd","Precision","Protection","PsychicVis","Quake Res","Rec Speed","Recoil","Recovery","Reload","ResistSts","Sharpness","ShortCharge","Shot Mix","Sleep","SleepCAdd","Sneak","Snow Res","Spc Attack","SpeedSetup","Stamina","SteadyHand","SwdShrpner","Sword Draw","Terrain","Throw","ThunderRes","Torso Inc","Tranquilzer","Water Res","Whim","Wide Area","WindPress"] })
    filteredArmors = Armors
});

app.get('/data', (req,res) => {
    res.json(filteredArmors)
    filteredArmors = [];
})

function checkSkill(armor, skill) {
    for (let x of [1,2,3,4,5]) {
        if (armor[`Skill${x}`] == skill)
            return true
    }
    return false
}


app.listen(process.env.PORT)
// app.listen(3000)